/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import model.User;

/**
 *
 * @author PC
 */
public class TestSelectUser {

    public static void main(String[] args) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String sql = "SELECT USER_ID,USER_NAME,USER_TEL,USER_PASSWORD FROM USER;";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("USER_ID");
                String name = result.getString("USER_NAME");
                String tel = result.getString("USER_TEL");
                String pass = result.getString("USER_PASSWORD");
                User user = new User(id, name, tel, pass);
                System.out.println(user);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQL");
        }
        db.close();
    }
}
