/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author PC
 */
public class TestUpdateProduct {
    public static void main(String[] args) {
      Connection con = null;
         Database db = Database.getInstance();
         con = db.getConnection();
        try {
            String sql = "UPDATE PRODUCT SET PD_NAME = ?,PD_PRICE = ? WHERE PD_ID = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, "Cocao");
            stmt.setDouble(2, 30);
            stmt.setInt(3, 4);
            int row = stmt.executeUpdate();
            System.out.println("Affect row: "+row);
        } catch (SQLException ex) {
            System.out.println("ERROR : SQL");
        }
       db.close();
    }
}
